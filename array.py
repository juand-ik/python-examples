def isInArray( arr, num, count ):
    return num in arr

if __name__ == '__main__':
    arr = [4, 5, 2, 4, 5, 9, 9, 4, 4]
    print( isInArray( arr, 90, 5 ) )
