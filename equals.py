def isInArray( arr, num, count ):
    if( num in arr ):
        total = 0
        for i in range( len(arr) ):
            if( arr[i] == num ):
                total += 1
        if( total == count ):
            return True
        else:
            return False


if __name__ == '__main__':
    arr = [4, 5, 2, 4, 5, 9, 9, 4, 4]
    print( isInArray( arr, 4, 4 ) )
