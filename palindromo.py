
def investString( input_string ):
    invest_string = ""
    string_len = len( input_string ) -1
    while( string_len >=0 ):
        invest_string += str( input_string[string_len] )
        string_len -= 1
    return invest_string

def isPalindrome( input_string ):
    new_string = str( input_string ).lower().replace(" ","")
    return str( new_string ) == investString( new_string )

if __name__ == '__main__':
    print( isPalindrome("Anita lava la tina") )
