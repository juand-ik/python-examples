def areEquals( arr ):
    pair = 0
    odd  = 0

    for i in range( len(arr) ):
        if( arr[i] % 2 == 0 ):
            pair += arr[i]
        else:
            odd  += arr[i]

    if( pair == odd ):
        return True
    else:
        return False


if __name__ == '__main__':
    arr = [ 10, 5, 5 ]
    print( areEquals( arr ) )
