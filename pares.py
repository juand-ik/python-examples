def isPair( input_raw ):
    try:
        value = int( input_raw )
        if( ( value % 2 ) == 0 ):
            return "It's pair"
        else:
            return "It's not pair"
        return value
    except ValueError:
        return "It's not number value"

if __name__ == '__main__':
    print( isPair(4) )
